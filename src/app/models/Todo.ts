export interface Todo {
    
    id?: String,
    title: String,
    description: String,
    dateToFinish: any,
    isFinished: Boolean
}