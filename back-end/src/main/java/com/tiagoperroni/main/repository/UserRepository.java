package com.tiagoperroni.main.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tiagoperroni.main.model.UserModel;

@Repository
public interface UserRepository extends JpaRepository<UserModel, Integer> {	
		
	public Optional<UserModel> findByLogin(String login);
}
