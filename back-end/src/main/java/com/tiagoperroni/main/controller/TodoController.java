package com.tiagoperroni.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tiagoperroni.main.model.Todo;
import com.tiagoperroni.main.service.TodoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "Todo")
@CrossOrigin("*")
@RestController
@RequestMapping("/todo")
public class TodoController {

	@Autowired
	private TodoService todoService;
	
	@ApiOperation(value = "Listar tarefas abertas")
	@GetMapping("/open")
	public ResponseEntity<List<Todo>> listOpen() {
		List<Todo> list = todoService.findAllOpen();
		return ResponseEntity.ok().body(list);
	}
	
	@ApiOperation(value = "Listar tarefas fechadas")
	@GetMapping("/closed")
	public ResponseEntity<List<Todo>> listClosed() {
		List<Todo> list = todoService.findAllClosed();
		return ResponseEntity.ok().body(list);
	}
	
	@ApiOperation(value = "Listar todos")
	@GetMapping
	public ResponseEntity<List<Todo>> listAll() {
		List<Todo> list =  todoService.listAll();
		return ResponseEntity.ok().body(list);
	}
	
	@ApiOperation(value = "Listar por código")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Todo> findById(@PathVariable Integer id) {
		return ResponseEntity.ok().body(todoService.findById(id));
	}
	
	@ApiOperation(value = "Criar")
	@PostMapping
	public ResponseEntity<Todo> create(@RequestBody Todo todo) {
		todo = todoService.create(todo);
		return new ResponseEntity<Todo>(todo, HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Atualizar")
	@PutMapping("/{id}")
	public ResponseEntity<Todo> update(@PathVariable Integer id, @RequestBody Todo todo) {
		Todo newObj = todoService.update(id, todo);
		return ResponseEntity.ok().body(newObj);
	}
	
	@ApiOperation(value = "Deletar")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		todoService.delete(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}	
	
}
