package com.tiagoperroni.main.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.springframework.stereotype.Service;

import com.tiagoperroni.main.model.Todo;
import com.tiagoperroni.main.repository.TodoRepository;

@Service
public class DBService {

	private final TodoRepository todoRepository;
	
	// @Autowired
	// private UserController userController;	

	public DBService(TodoRepository todoRepository) {	
		this.todoRepository = todoRepository;
	}
	
	public void InstanciaDB() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Todo t1 = new Todo(null, "Estudar","Estudar Java", sdf.parse("16/09/2021"), false);
		Todo t2 = new Todo(null, "Estudar","Estudar Angular", sdf.parse("16/09/2021"), false);
		
		// UserModel user = new UserModel(null, "tiago", "1234");
		
		// userController.salvar(user);
		
		todoRepository.saveAll(Arrays.asList(t1, t2));
		
	}
	
	
}
