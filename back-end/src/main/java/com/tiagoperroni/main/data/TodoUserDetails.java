package com.tiagoperroni.main.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import com.tiagoperroni.main.model.UserModel;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class TodoUserDetails implements UserDetails  {
    private static final long serialVersionUID = 1L;  

    private final Optional<UserModel> usuario;


    public TodoUserDetails(Optional<UserModel> usuario) {
        this.usuario = usuario;
    }
   

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new ArrayList<>();
    }

    @Override
    public String getPassword() {
        return usuario.orElse(new UserModel()).getPassword();
    }

    @Override
    public String getUsername() {
        return usuario.orElse(new UserModel()).getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
