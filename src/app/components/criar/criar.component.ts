import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Todo } from 'src/app/models/Todo';
import { TodoService } from 'src/app/service/todo.service';

@Component({
  selector: 'app-criar',
  templateUrl: './criar.component.html',
  styleUrls: ['./criar.component.css']
})
export class CriarComponent implements OnInit {

  todo: Todo = {
    title: '',
    description: '',
    dateToFinish: new Date(),
    isFinished: false
  }

  constructor(private router: Router, private service: TodoService) { }

  ngOnInit(): void {
  }

  create(): void {
    this.formataData();
    this.service.create(this.todo).subscribe((resposta) => {
      this.service.message('Todo criado com sucesso!');
      this.router.navigate(['']);      
    }, err => {
      this.service.message('Falha ao criar Todo!');
      this.router.navigate(['']);
    })
  }

  voltar(): void {
    this.router.navigate([''])
  }

  formataData(): void {
    let data = new Date(this.todo.dateToFinish)
    this.todo.dateToFinish = `${data.getDate()}/${data.getMonth() + 1}/${data.getFullYear()}`
  }

}