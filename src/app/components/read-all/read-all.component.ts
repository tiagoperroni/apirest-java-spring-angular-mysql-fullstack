import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Todo } from "src/app/models/Todo";
import { TodoService } from "src/app/service/todo.service";

@Component({
  selector: "app-read-all",
  templateUrl: "./read-all.component.html",
  styleUrls: ["./read-all.component.css"],
})
export class ReadAllComponent implements OnInit {
  closed = 0;

  list: Todo[] = [];
  listFinished: Todo[] = [];

  constructor(private service: TodoService, private router: Router) {}

  ngOnInit(): void {
    this.findAll();
  }

  findAll(): void {
    this.service.findAll().subscribe((res) => {
      res.forEach(todo => {
        if(todo.isFinished) {
          this.listFinished.push(todo);
        }else{
          this.list.push(todo);
        }
      })
     this.closed = this.listFinished.length;
    });
  }

  criarTodo(): void {
    this.router.navigate(['criar']);
  }

  finalizar(item: Todo): void {
    item.isFinished = true;
    this.service.update(item).subscribe((res) => {
      this.service.message('Tarefa finalizada com sucesso!');
      this.list = this.list.filter((todo) => todo.id === item.id);
      this.closed++
    });
  }

  delete(id: any): void {
    this.service.delete(id).subscribe((res) => {
      if(res === null){
        this.service.message('Task deletada com sucesso!');
        this.list = this.list.filter(todo => todo.id !== id);
      }
    });
  }

  navegarParaFinalizados(): void {
    this.router.navigate(['finalizados']);
  }

  countClosed(): void {
    for (let todo of this.list) {
      if (todo.isFinished) {
        this.closed++;
      }
    }
  }
}
