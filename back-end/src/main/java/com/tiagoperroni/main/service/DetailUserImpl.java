package com.tiagoperroni.main.service;

import java.util.Optional;

import com.tiagoperroni.main.data.TodoUserDetails;
import com.tiagoperroni.main.model.UserModel;
import com.tiagoperroni.main.repository.UserRepository;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class DetailUserImpl implements UserDetailsService {

    private final UserRepository repository;

    public DetailUserImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserModel> usuario = repository.findByLogin(username);
        if (usuario.isEmpty()) {
            throw new UsernameNotFoundException("Usuário [" + username + "] não encontrado");
        }

        return new TodoUserDetails(usuario);
    }

}
