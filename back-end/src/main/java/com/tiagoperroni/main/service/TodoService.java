package com.tiagoperroni.main.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tiagoperroni.main.exceptions.ObjectNotFountException;
import com.tiagoperroni.main.model.Todo;
import com.tiagoperroni.main.repository.TodoRepository;

@Service
public class TodoService {

	@Autowired
	private TodoRepository todoRepository;
	
	public List<Todo> findAllOpen() {
		List<Todo> list = todoRepository.findByIsFinished(false);
		return list;
	}
	
	public List<Todo> findAllClosed() {
		List<Todo> list = todoRepository.findByIsFinished(true);
		return list;
	}
	
	public List<Todo> listAll() {
		List<Todo> list = todoRepository.findAll();
		return list;
	}
	
	public Todo findById(Integer id) {
		Optional<Todo> todo = todoRepository.findById(id);
		return todo.orElseThrow(() -> new ObjectNotFountException("Todo não encontrado! Id fornecido: " + id));
	}
	
	public Todo create(Todo todo) {		
		if(todo == null) {
		 new ObjectNotFountException("Preencha todos os campos!");
		}
		todo.setId(null);
		return todoRepository.save(todo);
	}
	
	public void delete(Integer id) {
		findById(id);
		todoRepository.deleteById(id);
	}

	public Todo update(Integer id, Todo todo) {
		findById(id);
		Todo newObj = findById(id);
		System.out.println(newObj);
		BeanUtils.copyProperties(todo, newObj, "id");		
		return todoRepository.save(newObj);
	}
	
	
	
}
