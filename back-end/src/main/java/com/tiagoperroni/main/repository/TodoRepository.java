package com.tiagoperroni.main.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tiagoperroni.main.model.Todo;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Integer> {
	
	//@Query("SELECT obj FROM Todo WHERE obj.isFinished = false ORDER BY obj.dateToFinish")
	List<Todo> findByIsFinished(Boolean isFinished);
	
}
