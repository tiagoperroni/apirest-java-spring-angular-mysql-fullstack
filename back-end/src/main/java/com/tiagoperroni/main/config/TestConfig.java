package com.tiagoperroni.main.config;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.tiagoperroni.main.services.DBService;

@Configuration
@Profile("dev")
public class TestConfig {
	
	private final DBService DBSERVICE;

	public TestConfig(DBService dBSERVICE) {	
		DBSERVICE = dBSERVICE;
	}
	
	@Value("{spring.jpa.hibernate.dll-auto}")
	private String ddl;
	
	@Bean
	public void instanciaDB() throws ParseException {
		this.DBSERVICE.InstanciaDB();
	}
	
	
}
