import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Todo } from 'src/app/models/Todo';
import { TodoService } from 'src/app/service/todo.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  todo: Todo = {
    title: '',
    description: '',
    dateToFinish: new Date(),
    isFinished: false
  }

  constructor(private router: Router, private service: TodoService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.todo.id = this.route.snapshot.paramMap.get("id")!;
    this.findById();
  }

  findById(): void {
    this.service.findById(this.todo.id).subscribe((res) => {
      this.todo = res;
    });
  }

  update(): void {
    this.formataData();
    this.service.update(this.todo).subscribe((res) => {
    this.service.message('Todo atualizado com sucesso!');
    this.router.navigate(['']);
    }, err => {
      this.service.message('Falha ao atualizar todo!');
      this.router.navigate(['']);
    });    
  }

  voltar(): void {
    this.router.navigate(['']);
  }

  formataData(): void {
    let data = new Date(this.todo.dateToFinish)
    this.todo.dateToFinish = `${data.getDate()}/${data.getMonth() + 1}/${data.getFullYear()}`
  }

}
